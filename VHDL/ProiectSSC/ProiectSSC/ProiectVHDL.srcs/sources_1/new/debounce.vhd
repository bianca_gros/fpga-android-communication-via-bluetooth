----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.11.2020 09:14:20
-- Design Name: 
-- Module Name: debounce - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity debounce is
  Port (Clk: in std_logic;
        Rst: in std_logic;
        D_in: in std_logic;
        Q: out std_logic );
end debounce;

architecture Behavioral of debounce is

signal Q1, Q2, Q3 : std_logic:= '0';

begin

    process(Clk)
    begin
       if (Clk'event and Clk = '1') then
          if (Rst = '1') then
             Q1 <= '0';
             Q2 <= '0';
             Q3 <= '0';
          else
             Q1 <= D_IN;
             Q2 <= Q1;
             Q3 <= Q2;
          end if;
       end if;
    end process;
    
    Q <= Q1 and Q2 and (not Q3);
    
end Behavioral;
