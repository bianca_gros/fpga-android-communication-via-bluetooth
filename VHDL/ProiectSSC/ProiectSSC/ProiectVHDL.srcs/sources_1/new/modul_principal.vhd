----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.11.2020 11:22:11
-- Design Name: 
-- Module Name: modul_principal - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity modul_principal is
  Port (Clk: in std_logic;
        sw: in std_logic_vector(15 downto 0);
        Rst: in std_logic; 
        BTNU: in std_logic;
        BTND: in std_logic;
        BTNL: in std_logic;
        BTNR: in std_logic;
        Rx: in std_logic;
        Tx: out std_logic;
        TxRdy: out std_logic;
        RxRdy: out std_logic;
        An: out std_logic_vector(3 downto 0);
        Seg: out std_logic_vector(6 downto 0) );
end modul_principal;

architecture Behavioral of modul_principal is

signal button: STD_LOGIC:='0';
signal up: STD_LOGIC:='0';
signal down: STD_LOGIC:= '0';
signal left: STD_LOGIC:='0';
signal right: STD_LOGIC:='0';

signal TxData: STD_LOGIC_VECTOR(7 downto 0):=x"00";

signal outTx: std_logic:= '0';
signal outTxRdy: std_logic:= '0';

signal outRxData: std_logic_vector(7 downto 0):= x"00";
signal outRxDataF: std_logic_vector(7 downto 0):= x"00";
signal outRxRdy: std_logic:= '0';

signal Data: std_logic_vector(15 downto 0):= x"0000";

signal TxData1: STD_LOGIC_VECTOR(7 downto 0):=x"00";
signal TxData2: STD_LOGIC_VECTOR(7 downto 0):=x"00";

begin

    upbutton: entity WORK.debounce port map(Clk => Clk,
                                             Rst => Rst,
                                             D_IN => BTNU,
                                             Q => up);
                                             
    downbutton: entity WORK.debounce port map(Clk => Clk,
                                             Rst => Rst,
                                             D_IN => BTND,
                                             Q => down);
               
    leftbutton: entity WORK.debounce port map(Clk => Clk,
                                             Rst => Rst,
                                             D_IN => BTNL,
                                             Q => left);
                                             
    rightbutton: entity WORK.debounce port map(Clk => Clk,
                                             Rst => Rst,
                                             D_IN => BTNR,
                                             Q => right);
                                                          
    process(Clk)
    begin
        if RISING_EDGE(Clk)then
            if up = '1' then
                TxData1 <= x"55";
            elsif down = '1' then
                TxData1 <= x"44";
            elsif left = '1' then
                TxData1 <= x"4C";
            elsif right = '1' then
                TxData1 <= x"52";
            end if;  
        end if;
    
    end process;
    
    process(Clk)
    begin
        if rising_edge(Clk) then
        case sw is 
            when B"1000_0000_0000_0001" =>
                TxData2 <= x"31";
            when B"1000_0000_0000_0010" =>
                TxData2 <= x"32";
            when B"1000_0000_0000_0100" =>
                TxData2 <= x"33";
            when B"1000_0000_0000_1000" =>
                TxData2 <= x"34";
            when B"1000_0000_0001_0000" =>
                TxData2 <= x"35";
            when others => 
                TxData2 <= x"30";
        end case;
        end if;
    end process;
    
    TxData <= TxData2 when sw(15) = '1' else TxData1;
    
    button <= up or down or left or right;
    
    uart_tx1: entity WORK.uart_tx --generic(n => )
                              port map(Clk => Clk,
                                       Rst => Rst,
                                       Start => button,
                                       TxData => TxData,
                                       Tx => outTx,
                                       TxRdy => outTxRdy);
     
     
     uart_rx2: entity WORK.uart_rx port map (clk => Clk, 
                                             rx_serial => Rx,
                                             RX_DV => outRxRdy,
                                             RX_Byte => outRxData);
     
     
     

     Data <= "00000000" & outRxData;
     
     RxRdy <= outRxRdy;
     
     ssd: entity WORK.displ7seg port map (Clk => Clk,
                                          Rst => Rst,
                                          Data => Data,
                                          An => An,
                                          Seg => Seg);
                                       
     Tx <= outTx;
     TxRdy <= outTxRdy;
end Behavioral;
