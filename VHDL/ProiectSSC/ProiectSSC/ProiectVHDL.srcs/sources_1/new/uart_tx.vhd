library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;


entity uart_tx is
    generic(n: INTEGER:= 115_200);
    Port (Clk: in std_logic;
          Rst: in std_logic; 
          Start: in std_logic;
          TxData: in std_logic_vector(7 downto 0);
          Tx: out std_logic;
          TxRdy: out std_logic);
end uart_tx;

architecture Behavioral of uart_tx is

constant clock: INTEGER:= 100_000_000;
constant T_BIT: INTEGER:= clock / n;

type TYPE_STARE is (ready, load, send, waitbit, shift);
signal St: TYPE_STARE:= ready;

signal CntBit: INTEGER:= 0;
signal CntRate: INTEGER:= 0;

signal LdData: std_logic:= '0';
signal ShData: std_logic:= '0';
signal TxEn: std_logic:= '0';

signal TSR: std_logic_vector(9 downto 0):= "0000000000";

attribute keep : STRING;
attribute keep of St : signal is "TRUE";
attribute keep of CntRate : signal is "TRUE";
attribute keep of CntBit : signal is "TRUE";
attribute keep of TSR : signal is "TRUE";

begin

    proc_shift_register: process (Clk)
    
    begin
        if rising_edge(Clk) then
            if Rst = '1' then
                TSR <= conv_std_logic_vector(0, 10);
            else
                if LdData = '1' then
                    TSR <= '1' & TxData & '0';
                elsif ShData = '1' then
                    TSR <= '0' & TSR(9 downto 1);
                end if;
            end if;
        end if;    
    end process;


    proc_control: process (Clk) 
    
    begin
        if RISING_EDGE (Clk) then 
            if (Rst = '1') then
                St <= ready;
            else
                case St is
                    when ready =>
                        CntRate <= 0;
                        CntBit <= 0;
                        if (Start = '1') then
                            St <= load; 
                        end if;
                    when load => 
                        St <= send;
                    when send =>
                        CntBit <= CntBit + 1;
                        St <= waitbit;
                    when waitbit =>
                        CntRate <= CntRate + 1; 
                        if (CntRate = T_BIT - 3) then
                            CntRate <= 0;
                            St <= shift; 
                        end if;
                    when shift =>
                        --CntBit <= CntBit + 1; 
                        if (CntBit = 10) then
                            St <= ready;
                        else
                            St <= send; 
                        end if;
                    when others => 
                        St <= ready;
                end case;
            end if;
      end if;
   end process proc_control;
   
   
-- Setarea semnalelor de comanda
    LdData <= '1' when St = load else '0';
    ShData <= '1' when St = shift else '0';
    TxEn <= '0' when St = ready or St = load else '1';

-- Setarea semnalelor de iesire
    Tx <= TSR(0) when TxEn = '1' else '1'; 
    TxRdy <= '1' when St = ready else '0';

end Behavioral;
