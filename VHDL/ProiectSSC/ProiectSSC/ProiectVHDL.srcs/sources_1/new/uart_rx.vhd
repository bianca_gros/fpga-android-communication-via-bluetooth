------------------------------------------------------------------------------------
---- Company: 
---- Engineer: 
---- 
---- Create Date: 03.12.2020 16:18:28
---- Design Name: 
---- Module Name: uart_rx - Behavioral
---- Project Name: 
---- Target Devices: 
---- Tool Versions: 
---- Description: 
---- 
---- Dependencies: 
---- 
---- Revision:
---- Revision 0.01 - File Created
---- Additional Comments:
---- 
------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity UART_RX is
  generic (
    CLKS_PER_BIT : integer := 869    
    );
  port (
    Clk       : in  std_logic;
    RX_Serial : in  std_logic;
    RX_DV     : out std_logic;
    RX_Byte   : out std_logic_vector(7 downto 0)
    );
end UART_RX;


architecture Behavioral of UART_RX is

  type TYPE_STATE is (Idle, RX_Start_Bit, RX_Data_Bits,
                     RX_Stop_Bit, Cleanup);
  signal state : TYPE_STATE := Idle;
  

  signal Clk_Count : integer range 0 to CLKS_PER_BIT-1 := 0;
  signal Bit_Index : integer range 0 to 7 := 0; 
  signal sigRX_Byte   : std_logic_vector(7 downto 0) := (others => '0');
  signal sigRX_DV     : std_logic := '0';
  
begin

  automat: process (Clk)
  begin
    if rising_edge(Clk) then
      case state is
        when Idle =>
          sigRX_DV     <= '0';
          Clk_Count <= 0;
          Bit_Index <= 0;

          if RX_Serial = '0' then       
            state <= RX_Start_Bit;
          else
            state <= Idle;
          end if;
        when RX_Start_Bit =>
          if Clk_Count = (CLKS_PER_BIT-1)/2 then
            if RX_Serial = '0' then
              Clk_Count <= 0;
              state   <= RX_Data_Bits;
            else
              state   <= Idle;
            end if;
          else
            Clk_Count <= Clk_Count + 1;
            state   <= RX_Start_Bit;
          end if;
        when RX_Data_Bits =>
          if Clk_Count < CLKS_PER_BIT-1 then
            Clk_Count <= Clk_Count + 1;
            state   <= RX_Data_Bits;
          else
            Clk_Count            <= 0;
            sigRX_Byte(Bit_Index) <= RX_Serial;
            
            if Bit_Index < 7 then
              Bit_Index <= Bit_Index + 1;
              state   <= RX_Data_Bits;
            else
              Bit_Index <= 0;
              state   <= RX_Stop_Bit;
            end if;
          end if;
        when RX_Stop_Bit =>
          if Clk_Count < CLKS_PER_BIT-1 then
            Clk_Count <= Clk_Count + 1;
            state   <= RX_Stop_Bit;
          else
            sigRX_DV <= '1';
            Clk_Count <= 0;
            state   <= Cleanup;
          end if;
        when Cleanup =>
          state <= Idle;
          sigRX_DV   <= '0';
        when others =>
          state <= Idle;
      end case;
    end if;
  end process;

  RX_DV   <= sigRX_DV;
  RX_Byte <= sigRX_Byte;
  
end Behavioral;