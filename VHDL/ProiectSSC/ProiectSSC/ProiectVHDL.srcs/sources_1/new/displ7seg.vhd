library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity displ7seg is
   Port ( Clk  : in  STD_LOGIC;
          Rst  : in  STD_LOGIC;
          Data : in  STD_LOGIC_VECTOR (15 downto 0); 
          An   : out STD_LOGIC_VECTOR (3 downto 0); 
          Seg  : out STD_LOGIC_VECTOR (6 downto 0)); 
end displ7seg;

architecture Behavioral of displ7seg is

constant CLK_RATE  : INTEGER := 100_000_000;  
constant CNT_100HZ : INTEGER := 2**20;        
                                            
constant CNT_500MS : INTEGER := CLK_RATE / 2; 
signal Count       : INTEGER range 0 to CNT_100HZ - 1 := 0;
signal CountBlink  : INTEGER range 0 to CNT_500MS - 1 := 0;
--signal BlinkOn     : STD_LOGIC := '0';
signal CountVect   : STD_LOGIC_VECTOR (19 downto 0) := (others => '0');
signal LedSel      : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');
signal Digit1      : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
signal Digit2      : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
signal Digit3      : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
signal Digit4      : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');


signal Hex: std_logic_vector(3 downto 0):= "0000";

begin
   
   div_clk: process (Clk)
   begin
      if RISING_EDGE (Clk) then
         if (Rst = '1') then
            Count <= 0;
         elsif (Count = CNT_100HZ - 1) then
            Count <= 0;
         else
            Count <= Count + 1;
         end if;
      end if;
   end process div_clk;

   CountVect <= CONV_STD_LOGIC_VECTOR (Count, 20);
   LedSel <= CountVect (19 downto 17);

   Digit4 <= Data(15 downto 12);
   Digit3 <= Data(11 downto 8);
   Digit2 <= Data(7 downto 4);
   Digit1 <= Data(3 downto 0);

   An <= "1110" when LedSel = "000" else
         "1101" when LedSel = "001" else
         "1011" when LedSel = "010" else
         "0111" when LedSel = "011" else
         "1111";

    Hex <= Data (3  downto  0) when LedSel = "000" else
           Data (7  downto  4) when LedSel = "001" else
           Data (11 downto  8) when LedSel = "010" else
           Data (15 downto 12) when LedSel = "011" else
           "0000";
    
    Seg <= "1111001" when Hex = "0001" else            -- 1
           "0100100" when Hex = "0010" else            -- 2
           "0110000" when Hex = "0011" else            -- 3
           "0011001" when Hex = "0100" else            -- 4
           "0010010" when Hex = "0101" else            -- 5
           "0000010" when Hex = "0110" else            -- 6
           "1111000" when Hex = "0111" else            -- 7
           "0000000" when Hex = "1000" else            -- 8
           "0010000" when Hex = "1001" else            -- 9
           "0001000" when Hex = "1010" else            -- A
           "0000011" when Hex = "1011" else            -- b
           "1000110" when Hex = "1100" else            -- C
           "0100001" when Hex = "1101" else            -- d
           "0000110" when Hex = "1110" else            -- E
           "0001110" when Hex = "1111" else            -- F
           "1000000";                                  -- 0

end Behavioral;
